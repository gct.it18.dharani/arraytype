import java.util.Scanner;

public class ArrayType {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfElements = scanner.nextInt();
        int[] arrayElements = new int[numberOfElements];

        for (int i = 0; i < numberOfElements; i++) {
            arrayElements[i] = scanner.nextInt();
        }
        int type = typeOfArray(arrayElements, numberOfElements);
        System.out.println( type == 1 ? "Odd" : (type == 2 ? "Even" : "Mixed"));
    }

    private static int typeOfArray(int[] arrayElements, int numberOfElements) {
        int evenNumbersCount = 0, oddNumbersCount = 0;
        for(int i: arrayElements){
            if( i%2== 0 ) ++evenNumbersCount;
            else ++oddNumbersCount;
        }
        return (evenNumbersCount == numberOfElements ? 1 : (oddNumbersCount == numberOfElements ? 2 : 3 ));
    }
}

/* Input
        5
        55
        44
        35
        22
        15

    Output
        Mixed */

